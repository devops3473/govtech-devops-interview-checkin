module devops

go 1.12

require (
	github.com/ramya-rao-a/go-outline v0.0.0-20210608161538-9736a4bde949 // indirect
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.2.2
)
